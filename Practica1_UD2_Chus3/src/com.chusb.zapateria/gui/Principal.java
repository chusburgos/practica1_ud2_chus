package com.elenajif.vehiculosbbdd.gui;

/**
 * Created by Chus Burgos on 10/01/2021.
 */

public class Principal {
    public static void main(String args[]) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}