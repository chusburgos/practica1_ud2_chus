package com.elenajif.vehiculosbbdd.gui;

/**
 * Created by Chus Burgos on 11/01/2021.
 */
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Controlador implements ActionListener, TableModelListener{
    private Ventana ventana;
    private Modelo modelo;

    private enum tipoEstado { conectado, desconectado };
    private tipoEstado estado;

    public Controlador (Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.ventana = ventana;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        iniciarTabla1();
        addActionListener(this);
        addTableModelListeners(this);
    }

    private void addActionListener(ActionListener listener){
        vista.buscarBtn.addActionListener(listener);
        vista.eliminarBtn.addActionListener(listener);
        vista.nuevobtn.addActionListener(listener);
        vista.zapatosMarcaBtn.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
            System.out.println("actualizada");
            int filaModicada = e.getFirstRow();

            try {
                modelo.modificarZapato((Integer)ventana.dtm.getValueAt(filaModicada, 0),(String)ventana.dtm.getValueAt(filaModicada, 1),(String)ventana.dtm.getValueAt(filaModicada, 2),
                        (String)ventana.dtm.getValueAt(filaModicada, 3),(String)ventana.dtm.getValueAt(filaModicada, 4),(java.sql.Timestamp)ventana.dtm.getValueAt(filaModicada, 5), (String)ventana.dtm.getValueAt(filaModicada, 6));
                ventana.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch(comando){
            case "Zapatos por Marca":
                try {
                    modelo.zapatosPorMarca();
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "CrearTablaZapatos":
                try {
                    modelo.crearTablaCoches();
                    ventana.lblAccion.setText("Tabla zapatos creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "Nuevo":
                try {
                    modelo.insertarVehiculo(ventana.txtReferencia.getText(),ventana.txtMarca.getText(), ventana.txtTalla.getText(),
                            ventana.txtColor.getText(),ventana.dateTimePicker.getDateTimePermissive(), ventana.txtPrecio.getText());
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Buscar":
                //hacer el buscar
                break;
            case "Eliminar":
                try {
                    int filaBorrar = vista.tabla.getSelectedRow();
                    int idBorrar = (Integer) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarZapatos(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Conectar":
                if (estado == tipoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado=tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null, "Error de conexión", "Error", JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }

            } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado=tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null, "Error de desconexión", "Error", JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }


                }
                break;
        }
    }

    private void iniciarTabla() {
        String[] headers = { "id", "referencia", "marca", "talla", "color", "fecha matriculacion", "precio" };
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[5];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);
            fila[6]=resultSet.getObject(7);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + "filas cargadas");
        }


    }

    private void iniciarTabla1() {
        String[] headers = { "Cuantos", "marca" };
        vista.dtm1.setColumnIdentifiers(headers);
    }

    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[2];
        vista.dtm1.setRowCount(0);

        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);

            vista.dtm1.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + " filas cargadas");
        }


    }
}