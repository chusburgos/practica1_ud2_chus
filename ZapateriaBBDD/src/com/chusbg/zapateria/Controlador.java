package com.chusbg.zapateria;

/**
 * Created by Chus Burgos on 11/01/2021.
 */
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Controlador implements ActionListener, TableModelListener{
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado { conectado, desconectado };
    private tipoEstado estado;

    public Controlador (Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        iniciarTabla1();
        addActionListener(this);
        addTableModelListeners(this);
    }

    private void addActionListener(ActionListener listener) {
        vista.buscarBtn.addActionListener(listener);
        vista.eliminarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
        vista.zapatosMarcaBtn.addActionListener(listener);

        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    private void addTableModelListeners(TableModelListener listener) {
        vista.dtm.addTableModelListener(listener);
        vista.dtm.addTableModelListener(listener);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
            System.out.println("actualizada");
            int filaModicada = e.getFirstRow();

            try {
                modelo.modificarZapatos((Integer)vista.dtm.getValueAt(filaModicada, 0),(String)vista.dtm.getValueAt(filaModicada, 1),(String)vista.dtm.getValueAt(filaModicada, 2),
                        (String)vista.dtm.getValueAt(filaModicada, 3),(String)vista.dtm.getValueAt(filaModicada, 4), (String)vista.dtm.getValueAt(filaModicada, 5),
                        (java.sql.Timestamp)vista.dtm.getValueAt(filaModicada, 6), (String)vista.dtm.getValueAt(filaModicada, 7));
                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch(comando) {
            case "CrearTablaZapatos":
                try {
                    modelo.crearTablaZapatos();
                    vista.lblAccion.setText("Tabla zapatos creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Zapatos por Marca":
                try {
                    modelo.zapatosPorMarca();
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
                
            case "Zapatos por Referencia":
                try {
                    modelo.zapatosPorReferencia();
                    cargarFilas2(modelo.obtenerDatos2());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
                
            case "Nuevo":
                try {
                    modelo.insertarZapatos(vista.txtReferencia.getText(),vista.txtSexo.getText(), vista.txtMarca.getText(), vista.txtTalla.getText(),
                            vista.txtColor.getText(), vista.FechaDateTime.getDateTimePermissive(), vista.txtPrecio.getText());
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Buscar":
                //hacer el buscar
                break;
            case "Eliminar":
                try {
                    int filaBorrar = vista.tabla1.getSelectedRow();
                    int idBorrar = (Integer) vista.dtm.getValueAt(filaBorrar, 0);
                    modelo.eliminarZapatos(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila Eliminada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Conectar":
                if (estado == tipoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado = tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null, "Error de conexión", "Error", JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }

                } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado = tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null, "Error de desconexión", "Error", JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }
                }
                break;
        }
    }

    private void iniciarTabla() {
        String[] headers = { "id", "referencia", "sexo", "marca", "talla", "color", "fecha entrada", "precio" };
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[7];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + "filas cargadas");
        }


    }

    private void iniciarTabla1() {
        String[] headers = { "Cantidad", "Marca" };
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[2];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);

            vista.dtmZapatosMarca.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + " filas cargadas");
        }


    }

    private void iniciarTabla2() {
        String[] headers = { "Referencia", "Color" };
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void cargarFilas2(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[2];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);

            vista.dtmZapatosReferencia.addRow(fila);
        }
        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + " filas cargadas");
        }

    }
}