DELIMITER //
CREATE PROCEDURE crearTablaZapatos()
BEGIN
	CREATE TABLE zapatos(
		id int primary key auto_increment,
        referencia varchar(30) unique,
        sexo varchar(15),
        marca varchar (40),
        talla int,
        color varchar(40),
        fecha_entrada timestamp, 
        precio float);
END 