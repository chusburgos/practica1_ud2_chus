package com.chusbg.zapateria;

/**
 * Created by Chus Burgos on 10/01/2021.
 */

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    /**
     * En la clase Ventana.form se crea un cuadro que sera donde se empiece a realizar toda la interfaz de la aplicacion
     * por muy sencilla que sea. Dentro del cuadro se pondra un panel y ahí sera donde se añada las casillas de texto,
     * los diferentes botones, comboBox, DatePicker(la fecha de registro), el listado una vez se hayan introducido varios
     * productos y etiquetas, que depenpendiendo que modelo de zapato es variara la opcion de introducir los datos.
     */
    /**
     * Conforme se añaden los parametros en la clase Ventana.form se van declarando en esta clase (Ventana.java)
     */
    private JPanel Panel1;
    public JFrame frame;

    public JButton nuevoBtn;
    public JButton buscarBtn;
    public JButton eliminarBtn;
    public JButton zapatosMarcaBtn;

    public JTextField txtReferencia;
    public JTextField txtSexo;
    public JTextField txtMarca;
    public JTextField txtTalla;
    public JTextField txtColor;
    public JTextField txtPrecio;

    public JTextField txtBuscar2;
    public JTextField txtBuscar3;

    JLabel lblAccion;
    DefaultTableModel dtm;
    DefaultTableModel dtmZapatosMarca;
    DefaultTableModel dtmZapatosReferencia;

    JTable tabla1;
    JTable tabla2;
    JTable tabla3;
    DateTimePicker FechaDateTime;

    public JMenuItem itemConectar;
    public JMenuItem itemCrearTabla;
    public JMenuItem itemSalir;


    /**
     * Metodo que crea una ventana llamada Zapateria, donde se añade el panel se le van asignando una serie de normas,
     * como que cuando se le de la cruz la ventana se cierre. La ventana sera visible y coge todos los paquetes
     * para su funcionalidad, la localidad sera centrada
     */
    public Vista() {
        frame = new JFrame("Zapatería");
        frame.setContentPane(Panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm = new DefaultTableModel();
        tabla1.setModel(dtm);

        dtmZapatosMarca = new DefaultTableModel();
        tabla2.setModel(dtmZapatosMarca);

        dtmZapatosReferencia = new DefaultTableModel();
        tabla3.setModel(dtmZapatosReferencia);

        crearMenu();
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla Zapatos");
        itemCrearTabla.setActionCommand("CrearTablaZapatos");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }
}
