package com.chusbg.zapateria;

/**
 * Created by Chus Burgos on 12/01/2021.
 */
import javax.security.auth.callback.Callback;
import java.sql.*;
import java.time.LocalDateTime;

/**
 * Created by Chus Burgos on 13/01/2021.
 */
public class Modelo {
    private Connection conexion;

    public void crearTablaZapatos() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/zapatos","root","mysql");

        String sentenciaSql = "call crearTablaZapatos()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    public void conectar() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/zapatos","root","mysql");

    }

    public void desconectar() throws SQLException {
        conexion.close();
        conexion = null;
    }

    public ResultSet obtenerDatos() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta = "SELECT * FROM zapatos";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    public ResultSet obtenerDatos1() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta = "SELECT marca, COUNT(*) FROM zapatos GROUP BY marca, color";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public ResultSet obtenerDatos2() throws SQLException {
        if (conexion == null) {
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta = "SELECT referencia, color FROM zapatos GROUP BY referencia, sexo";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public int insertarZapatos(String referencia, String sexo, String marca, String talla, String color, LocalDateTime fechaEntrada, String precio) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta = "INSERT INTO zapatos(referencia, sexo, marca, talla, color, fecha_entrada, precio)" +
                "VALUES (?,?,?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, referencia);
        sentencia.setString(2, sexo);
        sentencia.setString(3, marca);
        sentencia.setString(4, talla);
        sentencia.setString(5, color);
        sentencia.setTimestamp(6, Timestamp.valueOf(fechaEntrada));
        sentencia.setString(7, precio);
        int numeroRegistros = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }
        return numeroRegistros;
    }

    public int eliminarZapatos(int id) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta = "DELETE FROM zapatos WHERE id =?";
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null) {
            sentencia.close();
        }
        return resultado;
    }

    public int modificarZapatos (int id, String referencia, String sexo, String marca, String talla, String color, Timestamp fechaEntrada, String precio) throws SQLException {
        if (conexion == null)
            return -1;
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE zapatos SET referencia =?, sexo =?,  marca =?, talla =?" +
                "color =?,fechaEntrada =?, precio =? WHERE id=?";

        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, referencia);
        sentencia.setString(2, sexo);
        sentencia.setString(3, marca);
        sentencia.setString(4, talla);
        sentencia.setString(5, color);
        sentencia.setTimestamp(6, fechaEntrada);
        sentencia.setString(7, precio);
        sentencia.setInt(8, id);

        int resultado = sentencia.executeUpdate();
        if (sentencia!=null) {
            sentencia.close();
        }
        return resultado;
    }

    public ResultSet buscarZapatosMarca(String marca) throws SQLException {
        if(conexion == null)
            return null;

        if(conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM zapatos WHERE marca =?, color =?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);

        sentencia.setString(1, String.valueOf(marca));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public ResultSet buscarZapatosReferencia(String referencia) throws SQLException {
        if(conexion == null)
            return null;

        if(conexion.isClosed())
            return null;

        String consulta = "SELECT * FROM zapatos WHERE referencia =?, sexo =?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, String.valueOf(referencia));
        ResultSet resultado = sentencia.executeQuery();

        return resultado;
    }

    public void zapatosPorMarca() throws SQLException {
        String sentenciaSql = "call mostrarZapatosMarca()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    public void zapatosPorReferencia() throws SQLException {
        String sentenciaSql = "call mostrarZapatosReferencia()";
        CallableStatement procedimiento = null;
        procedimiento = conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }
}
