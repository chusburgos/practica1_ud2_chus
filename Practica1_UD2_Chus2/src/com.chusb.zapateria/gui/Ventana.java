package gui;
/**
 * Created by Chus Burgos on 10/01/2021.
 */
import com.chusb.zapateria.base.Zapateria;
import com.github.lgooddatepicker.components.DatePicker;
import javafx.scene.input.InputMethodTextRun;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;

public class Ventana {
    /**
     * En la clase Ventana.form se crea un cuadro que sera donde se empiece a realizar toda la interfaz de la aplicacion
     * por muy sencilla que sea. Dentro del cuadro se pondra un panel y ahí sera donde se añada las casillas de texto,
     * los diferentes botones, comboBox, DatePicker(la fecha de registro), el listado una vez se hayan introducido varios
     * productos y etiquetas, que depenpendiendo que modelo de zapato es variara la opcion de introducir los datos.
     */
    /**
     * Conforme se añaden los parametros en la clase Ventana.form se van declarando en esta clase (Ventana.java)
     */
    ZapatosControlador zapato = new ZapatosControlador();
    private JPanel Panel1;
    public JFrame frame;

    public JButton nuevoBtn;
    public JButton buscarBtn;
    public JButton eliminarBtn;
    public JButton zapatosMarcaBtn;

    public JTextField txtReferencia;
    public JTextField txtSexo;
    public JTextField txtMarca;
    public JTextField txtColor;
    public DatePicker fechaEntradaPicker;
    public JTextField txtPrecio;
    public JTextField txtBuscar;
    public JLabel lblAccion;

    private JTable zapatosMarca;

    public DefaultTableModel dtm;
    public DefaultTableModel dtm1;
    public JMenuItem itemConectar;
    public JMenuItem itemCrearTabla;
    public JMenuItem itemSalir;
    /**
     * Metodo que crea una ventana llamada Zapateria, donde se añade el panel se le van asignando una serie de normas,
     * como que cuando se le de la cruz la ventana se cierre. La ventana sera visible y coge todos los paquetes
     * para su funcionalidad, la localidad sera centrada
     */
    public Ventana() {
        frame = new JFrame("Zapateria");
        frame.setContentPane(Panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm =new DefaultTableModel();
        tabla.setModel(dtm);

        dtm1 =new DefaultTableModel();
        cochesMarca.setModel(dtm1);

        crearMenu();
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla Zapatos");
        itemCrearTabla.setActionCommand("CrearTablaZapatos");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }
}
