CREATE DATABASE ZAPATERIA;
USE ZAPATERIA;

CREATE TABLE ZAPATOS (
id int primary key auto_increment,
referencia varchar(30) unique,
sexo varchar (15),
marca varchar (40),
talla int,
color varchar (40),
fecha_entrada timestamp,
precio float );

INSERT INTO ZAPATOS (referencia, sexo, marca, talla, color, fecha_entrada, precio)
 
VALUES('45663D', 'Mujer', 'Nike', 37, 'blanco','2017-05-06', 79.9);
 
select * from ZAPATOS;